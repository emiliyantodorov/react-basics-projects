import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import SingleCocktailPage from "./pages/SingleCocktailPage";
import GlobalProvider from "./globalContext";
import Footer from "./components/Footer";

const App = () => {

    return <Router>
        <GlobalProvider>
            <div className="d-flex flex-column min-vh-100">
                <NavBar/>
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route path="/cocktails/:id">
                        <SingleCocktailPage/>
                    </Route>
                </Switch>
                <Footer/>
            </div>
        </GlobalProvider>
    </Router>;
};

export default App;