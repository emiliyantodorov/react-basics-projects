import React from 'react';
import Card from "react-bootstrap/Card";
import Badge from "react-bootstrap/cjs/Badge";

const SingleCocktailCard = ({ name, imgURL, type, ingredients }) => {
  return <Card>
    <Card.Img src={imgURL}/>
    <Card.Body>
      <Card.Title><Badge bg="secondary">Name:</Badge> {name}</Card.Title>
      <Card.Title><Badge bg="secondary">Type:</Badge> {type}</Card.Title>
      <Card.Title><Badge bg="secondary">Ingredients:</Badge> {ingredients}</Card.Title>
    </Card.Body>
  </Card>;
};

export default SingleCocktailCard;