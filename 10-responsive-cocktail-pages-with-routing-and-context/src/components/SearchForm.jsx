import React, { useContext, useState } from 'react';
import Form from "react-bootstrap/Form";
import { GlobalContext } from "../globalContext";

const SearchForm = () => {
  const { searchedCocktail, setSearchedCocktail } = useContext(GlobalContext);

  const changeHandler = evt => setSearchedCocktail(evt.target.value);

  return <div className="row">
    <div className="col col-md-6 d-inline-block m-auto">
      <Form className="mb-5">
        <Form.Control value={searchedCocktail}
                      onChange={changeHandler}
        />
      </Form>
    </div>
  </div>
};

export default SearchForm;