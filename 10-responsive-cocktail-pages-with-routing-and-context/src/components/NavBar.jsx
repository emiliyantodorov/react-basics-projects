import React from 'react';

import Container from "react-bootstrap/cjs/Container";

import {Link} from "react-router-dom";

const NavBar = () => {
    return <nav className="shadow py-3">
        <Container>
            <div className="d-flex justify-content-between">
                <Link to="/">
                    <div className="logo-box mx-3">LOGO</div>
                </Link>

                <ul className="list-unstyled m-0 d-flex">
                    <li><Link to="/">Home</Link></li>
                    <li className="mx-3"><a href="#">About</a></li>
                </ul>
            </div>
        </Container>
    </nav>;
};

export default NavBar;