import { createContext, useState } from "react";

export const GlobalContext = createContext({
  cocktails: [],
  setCocktails: () => {},
  searchedCocktail: "",
  setSearchedCocktail: () => {},
  isLoading: true,
  setIsLoading: () => {}
});

const GlobalProvider = ({ children }) => {
  const [ cocktails, setCocktails ] = useState([]);
  const [ searchedCocktail, setSearchedCocktail ] = useState("");
  const [ isLoading, setIsLoading ] = useState(true);

  return <GlobalContext.Provider value={{
    cocktails,
    setCocktails,
    searchedCocktail,
    setSearchedCocktail,
    isLoading,
    setIsLoading,
  }}>
    {children}
  </GlobalContext.Provider>;
};

export default GlobalProvider;