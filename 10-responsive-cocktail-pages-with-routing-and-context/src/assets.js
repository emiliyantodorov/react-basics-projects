export const URL = "https://www.thecocktaildb.com/api/json/v1/1";

// Lookup full cocktail details by id
export const singleCocktailURL = `${URL}/lookup.php?i=`;

// Search cocktail by name
export const searchCocktailByNameURL = `${URL}/search.php?s=`;