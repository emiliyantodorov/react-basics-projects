import React from 'react';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/cjs/Button";
import { Link } from "react-router-dom";

const CocktailCard = ({ id, name, imgURL }) => {

  return <Card>
    <Card.Img src={imgURL}/>
    <Card.Body>
      <Card.Title>{name}</Card.Title>
    </Card.Body>
    <Card.Footer>
      <Link to={`/cocktails/${id}`}>
        <Button className="w-100">Read More</Button>
      </Link>
    </Card.Footer>
  </Card>;
};

export default CocktailCard;