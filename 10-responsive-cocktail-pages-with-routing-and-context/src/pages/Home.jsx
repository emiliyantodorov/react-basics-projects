import React, {useContext, useEffect, useState} from 'react';
import Container from "react-bootstrap/cjs/Container";
import {searchCocktailByNameURL} from "../assets";
import {GlobalContext} from "../globalContext";
import CocktailCard from "./CocktailCard";
import SearchForm from "../components/SearchForm";
import Spinner from "react-bootstrap/cjs/Spinner";
import Alert from "react-bootstrap/Alert";

const Home = () => {
    const {cocktails, setCocktails, searchedCocktail} = useContext(GlobalContext);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        searchByName();
    }, []);

    useEffect(() => {
        setError(null);
        setIsLoading(false);

        if (!searchedCocktail) return;

        setIsLoading(true);

        const timerId = setTimeout(() => {
            searchByName();
        }, 1000);

        return () => clearTimeout(timerId);
    }, [searchedCocktail]);

    const searchByName = () => {
        fetch(`${searchCocktailByNameURL}${searchedCocktail}`)
            .then(res => res.json())
            .then(data => {
                setIsLoading(false);

                const {drinks} = data;

                if (!drinks) {
                    setError("No cocktails matched");
                    setCocktails([]);

                    return;
                }

                setError(null);

                setCocktails(drinks.map(curCocktail => {
                        const {idDrink: id, strDrink: name, strDrinkThumb: imgURL} = curCocktail;

                        return {id, name, imgURL};
                    }
                ));
            })
            .catch(err => {
                console.log(err);
            });
    }

    return <section className="flex-grow-1 section-home py-5">
        <Container>
            <h2 className="text-center mb-5">Cocktails</h2>
            <SearchForm/>
            {error && <Alert variant="danger" className="text-center">{error}</Alert>}
            {
                isLoading && <div className="text-center">
                    <Spinner animation="border" role="status" className="mb-4">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            }
            <div className="row justify-content-center">
                {
                    cocktails.map(curCocktail =>
                        <div key={curCocktail.id} className="mb-4 col-10 col-sm-6 col-md-4 col-lg-3">
                            <CocktailCard {...curCocktail}/>
                        </div>)
                }
            </div>
        </Container>
    </section>;
};

export default Home;