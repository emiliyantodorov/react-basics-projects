import React, {useEffect, useState} from 'react';
import {singleCocktailURL} from "../assets";
import {useParams} from "react-router-dom";
import SingleCocktailCard from "../components/SingleCocktailCard";
import Container from "react-bootstrap/cjs/Container";
import Spinner from "react-bootstrap/cjs/Spinner";

const SingleCocktailPage = () => {
    const {id} = useParams();
    const [cocktail, setCocktail] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {

        fetch(`${singleCocktailURL}${id}`)
            .then(res => res.json())
            .then(({drinks}) => {
                setIsLoading(false);

                const {
                    idDrink: id,
                    strDrink: name,
                    strDrinkThumb: imgURL,
                    strAlcoholic: type,
                } = drinks[0];

                const ingredients = Object.keys(drinks[0])
                    .filter(propName => propName.startsWith("strIngredient", 0) && drinks[0][propName] !== null)
                    .map(propName => drinks[0][propName]);

                setCocktail({id, name, imgURL, type, ingredients});
            })
            .catch(err => {
                setIsLoading(false);
                console.log(err);
            });
    }, [id]);

    return (
        <>
            <section className="section-single-cocktail py-5 flex-grow-1">
                <Container>
                    <h2 className="mb-5 text-center">Cocktail Details</h2>
                    {isLoading && <div className="text-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                    }
                    {
                        cocktail && <div className="row justify-content-center">
                            <div className="col-12 col-sm-8 col-md-6 col-lg-4">
                                <SingleCocktailCard {...cocktail}/>
                            </div>
                        </div>
                    }
                </Container>
            </section>

        </>
    )
        ;
};

export default SingleCocktailPage;