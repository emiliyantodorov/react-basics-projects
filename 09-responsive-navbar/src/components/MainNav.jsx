import React, { useEffect, useRef, useState } from 'react';
import { mainLinks } from "../data";
import Container from "react-bootstrap/cjs/Container";
import Button from "react-bootstrap/Button";
import "./MainNav.css";

const MainNav = () => {
  const [ showMobileMenu, setShowMobileMenu ] = useState(false);
  const mainNavBarRef = useRef();

  useEffect(() => {
    if (!showMobileMenu) {
      mainNavBarRef.current.style.height = "0";

      return;
    }


    mainNavBarRef.current.style.height = `${mainNavBarRef.current.scrollHeight}px`;
  }, [ showMobileMenu ]);

  const toggleMobileMenuClickHandler = () => {
    setShowMobileMenu(!showMobileMenu);
  };

  return (
    <nav className="main-navbar position-relative shadow-sm py-3">
      <Container className="d-flex justify-content-between align-items-center">
        <div className="logo">LOGO</div>

        <ul
          ref={mainNavBarRef}
          className="main-nav align-items-md-center my-md-0 list-unstyled">
          {
            mainLinks.map(({ id, title }) =>
              <li key={id} className="main-nav__item mx-2">
                <a href="#" className="main-nav--link py-1 d-inline-block py-md-0">{title}</a>
              </li>)
          }
        </ul>

        <Button onClick={toggleMobileMenuClickHandler} className="mobile-btn d-md-none">
          M
        </Button>
      </Container>
    </nav>
  );
};

export default MainNav;