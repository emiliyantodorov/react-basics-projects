import Container from "react-bootstrap/cjs/Container";
import { useState } from "react";
import Recipe from "./Recipe";
import Spinner from "react-bootstrap/Spinner";
import Categories from "./Categories";

const url = "https://forkify-api.herokuapp.com/api/search?q=";
const categories = [ ...new Set([ "pizza", "hamburger", "pudding", "seafood" ]) ];

const App = () => {
  const [ recipes, setRecipes ] = useState([]);
  const [ error, setError ] = useState(null);
  const [ isLoading, setIsLoading ] = useState(false);

  const fetchItems = async query => {
    setIsLoading(true);

    try {
      const res = await fetch(`${url}${query}`);
      setIsLoading(false);

      if (!res.ok) {
        setError("No results");

        return;
      }

      const data = await res.json();

      if (error) setError(null);

      setRecipes(data.recipes);
    } catch (e) {
      setIsLoading(false);
      setError("Something went wrong");
    }
  };

  return (
    <>
      <section className="section-meals">
        <Container>
          <h2 className="my-5 text-center">Our Menu</h2>
          <div className="text-center">
            <Categories categories={categories} fetchItems={fetchItems}/>
          </div>

          <div className="items">
            <div className="text-center">
              {isLoading && <Spinner animation="border"/>}
            </div>
            {
              !isLoading &&
              <div className="row">
                {recipes.map(curRecipe => <Recipe key={curRecipe.recipe_id} {...curRecipe}/>)}
              </div>
            }
          </div>
        </Container>
      </section>
    </>
  );
};

export default App;