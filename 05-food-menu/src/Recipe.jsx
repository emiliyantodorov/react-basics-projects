import Card from "react-bootstrap/Card";

const Recipe = ({ image_url, title }) => {
  return (
    <div className="col-3 mb-3">
      <Card>
        <Card.Img src={image_url}/>
        <Card.Body>
          <Card.Title>{title}</Card.Title>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Recipe;