import ButtonGroup from "react-bootstrap/cjs/ButtonGroup";
import Button from "react-bootstrap/cjs/Button";

const Categories = ({ categories, fetchItems }) => {


  return (
    <ButtonGroup className="mb-5">
      {
        categories.map((curCategory, index) =>
          <Button key={index} variant="secondary" onClick={() => fetchItems(curCategory)}>{curCategory}</Button>
        )
      }
    </ButtonGroup>
  );
};

export default Categories;