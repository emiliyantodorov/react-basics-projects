class Job {
  #id;
  #name;
  #pros;

  constructor(id, name, pros) {
    this.#id = id;
    this.#name = name;
    this.#pros = pros;
  }

  get id() {
    return this.#id;
  }

  get name() {
    return this.#name;
  }

  get pros() {
    return [ ...this.#pros ];
  }
}

export const jobs = [
  new Job(1, "Bookmaker", [
    "Work under pressure",
    "Fast decisions",
  ]),
  new Job(2, "Shop Seller", [
    "Teamwork experience",
    "Making fast decisions",
    "Work with clients",
  ]),
  new Job(3, "Front End Dev", [
    "Teamwork",
    "Work under pressure",
  ])
];