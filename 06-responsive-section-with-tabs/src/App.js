import ExperienceSection from "./ExperienceSection";

const App = () => {

  return (
    <>
      <ExperienceSection/>
    </>
  );
};

export default App;