const JobsList = ({ jobs, activeJobIndex, onClick }) => {


  return (
    <ul className="d-flex justify-content-between flex-md-column">
      {
        jobs.map(({ id, name }, index) => {
          return <li key={id}
                     className={`px-md-5 h3 job-titles__item  ${activeJobIndex === index ? "cta--active-job text-info" : null}`}
                     onClick={onClick.bind(null, index)}
          >
            {name}
          </li>;
        })
      }
    </ul>
  );
};

export default JobsList;