import { useState } from "react";
import Container from "react-bootstrap/cjs/Container";
import { jobs } from "./data";
import JobsList from "./JobsList";
import JobExpInfo from "./JobExpInfo";


const ExperienceSection = () => {
  const [ myJobs, setMyJobs ] = useState(jobs);
  const [ activeJobIndex, setActiveJobIndex ] = useState(0);
  console.log(myJobs[activeJobIndex]);
  const jobClickHandler = index => setActiveJobIndex(index);

  return (
    <section className="section-experience">
      <h2 className="text-center my-5">Experience</h2>

      <Container>
        <div className="row flex-column flex-md-row">
          <div className="col-12 mb-5 col-md-6 col-lg-4">
            <JobsList jobs={myJobs} activeJobIndex={activeJobIndex} onClick={jobClickHandler}/>
          </div>
          <div className="col-12 col-md-6 col-lg-8">
            <JobExpInfo pros={myJobs[activeJobIndex].pros}/>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default ExperienceSection;