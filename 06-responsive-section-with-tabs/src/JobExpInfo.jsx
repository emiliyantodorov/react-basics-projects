import { TiArrowForwardOutline } from "react-icons/ti";

const JobExpInfo = ({ pros }) => {

  return <ul>
    {
      pros.map((curInfo, index) => <li key={index} className="h4"><TiArrowForwardOutline
        className="text-info"/> {curInfo}</li>)
    }
  </ul>;
};

export default JobExpInfo;