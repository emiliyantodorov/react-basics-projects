import Container from "react-bootstrap/cjs/Container";
import Card from "react-bootstrap/Card";
import { useState } from "react";
import { GrAddCircle } from "react-icons/gr";
import Button from "react-bootstrap/cjs/Button";
import Question from "./Question";

const mockData = [
  { id: 1, q: "What is your name?", a: "My name is JS!" },
  { id: 2, q: "What is closure?", a: "I don't know, and I am sure you, don't know too :D" },
  { id: 3, q: "How the callstack works?", a: "FILO !!!" },
];

const App = () => {

  return (
    <>
      <section className="section-accordion">
        <h2 className="my-5 text-center">Questions & Answers</h2>

        <Container>
          <div className="questions">
            {mockData.map(data => <Question key={data.id} {...data}/>)}
          </div>
        </Container>
      </section>
    </>
  );
};

export default App;