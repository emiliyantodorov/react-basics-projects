import Card from "react-bootstrap/Card";
import { GrAddCircle, } from "react-icons/gr";
import { MdRemoveCircleOutline } from "react-icons/md";
import { useState } from "react";

const Question = ({ q, a }) => {
  const [ isExpanded, setIsExpanded ] = useState(false);

  return (
    <Card
      className="mb-3"
      style={{ maxWidth: "30rem", margin: "0 auto" }}>
      <Card.Body>
        <Card.Title className="d-flex justify-content-between">
          {q}
          <span onClick={() => setIsExpanded(!isExpanded)}>
            {!isExpanded && <GrAddCircle/>}
            {isExpanded && <MdRemoveCircleOutline/>}
          </span>
        </Card.Title>
        {isExpanded && <Card.Text>{a}</Card.Text>}
      </Card.Body>
    </Card>
  );
};

export default Question;