Simple TODO app, where you can:
* add task
* delete task
* edit task
* clear the list with items

Tasks are saved in the localStorage so that when refreshing the page all tasks remains there.