import React, { useState } from 'react';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/cjs/Button";
import { FaRegEdit } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";

const Todo = ({ id, content, onEdit, onDelete }) => {
  const [ value, setValue ] = useState(content);
  const [ isEditing, setIsEditing ] = useState(false);

  const toggleIsEditing = () => setIsEditing(!isEditing);

  const submitEditHandler = evt => {
    evt.preventDefault();

    onEdit(id, value) && toggleIsEditing();
  };

  const editingClickHandler = () => {

    toggleIsEditing();
  };

  const deleteClickHandler = () => onDelete(id);

  return (
    <li className="mb-3">
      {!isEditing && <p className="d-flex justify-content-between">
        {content}
        <span>
          <Button onClick={editingClickHandler} className="me-1"><FaRegEdit/></Button>
          <Button onClick={deleteClickHandler} variant="danger"><RiDeleteBin6Line/></Button>
        </span>
      </p>}
      {isEditing && <div>
        <Form onSubmit={submitEditHandler}>
          <Form.Control type="text" onChange={evt => setValue(evt.target.value)} value={value}/>
          <span>
            <Button type="submit">Edit</Button>
            <Button onClick={toggleIsEditing} variant="danger">Cancel</Button>
          </span>
        </Form>
      </div>
      }
    </li>
  );
};

export default Todo;