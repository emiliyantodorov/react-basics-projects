import React, { useEffect } from 'react';
import Alert from "react-bootstrap/Alert";

const AlertComponent = ({ message, variant, onRemove, todos }) => {
  useEffect(() => {
    const timerId = setTimeout(() => {
      onRemove();
    }, 2000);

    return () => clearTimeout(timerId);
  }, [todos]);


  return <Alert variant={variant} className="py-1 text-center">
    {message}
  </Alert>;
};

export default AlertComponent;