import React from 'react';
import Todo from "./Todo";

const TodoList = ({ todos, onEdit, onDelete }) => {

  return (
    <ul className="mb-5">
      {
        todos.map(todo => <Todo key={todo.id} onEdit={onEdit} onDelete={onDelete}  {...todo}/>)
      }
    </ul>
  );
};

export default TodoList;