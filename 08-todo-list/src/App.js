import Container from "react-bootstrap/cjs/Container";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

import { v4 as uuidv4 } from "uuid";

import TodoList from "./components/TodoList";

import { useEffect, useState } from "react";

import AlertComponent from "./components/Alert";

const App = () => {
  const [ todos, setTodos ] = useState(JSON.parse(localStorage.getItem("todos")) || []);
  const [ todoValue, setTodoValue ] = useState("");
  const [ alert, setAlert ] = useState(null);

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [ todos ]);

  const addClickHandler = () => {
    if (!todoValue) return setAlert({ message: "Wrong input", variant: "danger" });

    setTodos(prevTodos => [ ...prevTodos, { id: uuidv4(), content: todoValue } ]);
    setAlert({ message: "Added", variant: "success" });
  };

  const editClickHandler = (id, updatedContent) => {
    if (!updatedContent) {
      setAlert({ message: "Wrong input", variant: "danger" });

      return false;
    }

    setTodos(prevTodos => prevTodos.map(todo => todo.id === id ? {
      id,
      content: updatedContent
    } : todo));
    setAlert({ message: "Edited", variant: "primary" });

    return true;
  };

  const deleteItemHandler = id => {
    setTodos(prevTodos => prevTodos.filter(curTodo => curTodo.id !== id));
    setAlert({ message: "Deleted", variant: "danger" });
  };

  const emptyListHandler = () => {
    setTodos([]);
    setAlert({ message: "Empty list", variant: "danger" });
  };

  const removeAlert = () => setAlert(null);

  return (
    <>
      <section className="section-todos">
        <Container>
          <div className="row justify-content-center">
            <div className="col-12 col-lg-6">
              <h2 className="text-center my-5">To Do List</h2>

              {alert && <AlertComponent onRemove={removeAlert} todos={todos} {...alert}/>}

              <InputGroup className="mb-3">
                <FormControl
                  onChange={evt => setTodoValue(evt.target.value)}
                  value={todoValue}
                  placeholder="Enter task..."
                  aria-label="Enter task..."
                  aria-describedby="basic-addon2"
                />
                <Button onClick={addClickHandler} variant="outline-secondary" id="button-addon2">
                  Add
                </Button>
              </InputGroup>

              <TodoList todos={todos} onEdit={editClickHandler} onDelete={deleteItemHandler}/>

              {todos.length > 0 &&
              <div className="text-center"><Button onClick={emptyListHandler} variant="danger">Empty List</Button>
              </div>}
            </div>
          </div>
        </Container>
      </section>
    </>
  );
};

export default App;