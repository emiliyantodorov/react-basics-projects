import { Card, Button } from "react-bootstrap";
import { useState } from "react";

const PhotoCard = ({ id, title, url, onDelete }) => {
  const [ isShowingMore, setIsShowingMore ] = useState(false);

  const content = "Some quick example text to build on the card title and make up the bulk of\n" +
    "          the card's content.";

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={url}/>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>
          {!isShowingMore ? content.slice(0, 10).concat("...") : content}
        </Card.Text>
        <Button variant="primary"
                onClick={() => setIsShowingMore(!isShowingMore)}>{!isShowingMore ? "Read More" : "Show Less"}</Button>
      </Card.Body>
      <Card.Footer>
        <Button variant="danger" onClick={onDelete.bind(null, id)}>Delete Card</Button>
      </Card.Footer>
    </Card>
  );
};

export default PhotoCard;