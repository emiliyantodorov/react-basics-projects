import {ListGroup, ListGroupItem} from "react-bootstrap";
import PhotoCard from "./PhotoCard";

const List = ({items, onDelete}) => {

    return (
        <ListGroup as="ul" className="mb-3">
            {
                items.map(item =>
                    <ListGroupItem key={item.id}>
                        <PhotoCard onDelete={onDelete} {...item}/>
                    </ListGroupItem>
                )
            }
        </ListGroup>
    )
}

export default List;