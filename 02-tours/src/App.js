import Container from "react-bootstrap/cjs/Container";
import List from "./components/List";
import { useFetch } from "./hooks/useFetch";

const url = "https://jsonplaceholder.typicode.com/albums/1/photos";
/*{
"albumId": 1,
"id": 2,
"title": "reprehenderit est deserunt velit ipsam",
"url": "https://via.placeholder.com/600/771796",
"thumbnailUrl": "https://via.placeholder.com/150/771796"
},*/

const App = () => {
  const { data, setData, isLoading, error } = useFetch(url);

  const deleteCardHandler = id => setData(data.filter(curCard => curCard.id !== id));

  return (
    <div>
      <section className="section-tours">
        <Container>
          <h2 className="text-center">Tours</h2>
          {isLoading && "Loading..."}
          {error && <div className="alert-warning">{error}</div>}
          <List items={data} onDelete={deleteCardHandler}/>
        </Container>
      </section>
    </div>
  );
};

export default App;