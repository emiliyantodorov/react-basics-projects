import Card from "react-bootstrap/Card";

const ReviewCard = ({ index, thumbnailUrl, title }) => {
  return <Card style={{ transform: `translateX(calc(100% * ${index}))` }}
               className="position-absolute w-100 text-center border-0"
  >
    <Card.Body>
      <Card.Title className="mb-5">
        <img src={thumbnailUrl} alt=""/>
      </Card.Title>
      <Card.Text>{title}</Card.Text>
    </Card.Body>
  </Card>;
};

export default ReviewCard;