import { useEffect, useState } from "react";
import "./index.css";
import Button from "react-bootstrap/cjs/Button";
import ReviewCard from "./ReviewCard";

const url = "https://jsonplaceholder.typicode.com/albums/1/photos";

const App = () => {
  const [ cardData, setCardData ] = useState([]);
  const [ curItemIndex, setCurItemIndex ] = useState(0);

  const prev = () => setCurItemIndex(prevState => prevState + 1);
  const next = () => setCurItemIndex(prevState => prevState - 1);

  const getData = async url => {
    const data = await fetch(url).then(res => res.json());

    setCardData(data);
  };

  useEffect(() => {
    getData(url);
  }, []);

  return (
    <>
      <div className="app">
        <section className="d-flex justify-content-center align-items-center vh-100">
          <Button onClick={prev} className="align-self-center mx-3" disabled={curItemIndex === 0 && true}>Prev</Button>
          <div className="main-box overflow-hidden">
            <div className="main-box__slider d-flex align-items-center h-100"
                 style={{ transform: `translateX(calc(100% * ${curItemIndex}))` }}
            >
              {
                cardData.map((curData, index) => <ReviewCard key={curData.id}
                                                             index={index}
                                                             {...curData}/>
                )
              }
            </div>
          </div>
          <Button onClick={next} className="align-self-center mx-3"
                  disabled={curItemIndex === cardData.length - 1 && true}>Next</Button>
        </section>
      </div>
    </>
  );
};

export default App;