import Container from "react-bootstrap/cjs/Container";
import { useFetch } from "./hooks/useFetch";
import "./App.css";
import { FaChevronLeft, FaChevronRight, FaQuoteRight } from "react-icons/fa";
import { useState } from "react";
import Button from "react-bootstrap/cjs/Button";

const url = "https://jsonplaceholder.typicode.com/albums/1/photos";
/*albumId: 1
id: 1
thumbnailUrl: "https://via.placeholder.com/150/92c952"
title: "accusamus beatae ad facilis cum similique qui sunt"
url: "https://via.placeholder.com/600/92c952"*/

const App = () => {
  const { data, isLoading, error } = useFetch(url);
  const [ curIndex, setCurIndex ] = useState(0);

  const prevRev = () => {
    if (curIndex === 0) return;

    setCurIndex(prevState => prevState - 1);
  };

  const nextRev = () => {
    if (curIndex === data.length - 1) return;

    setCurIndex(prevState => prevState + 1);
  };

  const randomRev = () => setCurIndex(Math.floor(Math.random() * data.length - 1))
  return (
    <div>
      <section className="section-tours">
        <Container>
          <h2 className="text-center mb-5">Reviews</h2>

          <div className="bg-secondary d-flex flex-column align-items-center justify-content-center py-3">
            {isLoading && <h2 className="text-white">Loading...</h2>}
            {data.length > 0 &&
            <div className="review">
              <div className="review__heading position-relative d-flex justify-content-center mb-3">
                <div className="quote-icon p-1 rounded-circle bg-white position-absolute align-top">
                  <FaQuoteRight/>
                </div>
                <img src={data[curIndex].thumbnailUrl} alt="" className="rounded-circle"/>
              </div>
              <div className="review__body text-white">
                {data[curIndex].title}
              </div>
              <div className="review__footer text-center pt-3 mb-3">
                <Button className="p-3 d-inline-block rounded-2 mx-2"
                        onClick={prevRev}
                        disabled={curIndex === 0}
                >
                  <FaChevronLeft/>
                </Button>
                <Button className="p-3 d-inline-block rounded-2 mx-2"
                        onClick={nextRev}
                        disabled={curIndex === data.length - 1}
                >
                  <FaChevronRight/>
                </Button>
              </div>
              <div className="text-center">
                <Button variant="success" onClick={randomRev}>Random Review</Button>
              </div>
            </div>
            }
          </div>
        </Container>
      </section>
    </div>
  );
};

export default App;