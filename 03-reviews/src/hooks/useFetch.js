import { useEffect, useState } from "react";

export const useFetch = (url) => {
  const [ data, setData ] = useState([]);
  const [ error, setError ] = useState(null);
  const [ isLoading, setIsLoading ] = useState(true);

  const getData = async () => {
    try {
      const res = await fetch(url);

      setIsLoading(false);

      if (!res.ok) {
        setError("Bad Request!");

        return;
      }

      const receivedData = await res.json();
      setError(null);
      setData(receivedData);
    } catch (e) {
      setError(e.message);
    }
  };

  useEffect(() => {
    getData();
  }, [ url ]);

  return { data, setData, isLoading, error };
};