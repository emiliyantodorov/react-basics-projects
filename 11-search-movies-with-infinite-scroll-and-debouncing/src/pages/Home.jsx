import React, {useEffect, useState} from 'react';
import Container from "react-bootstrap/cjs/Container";
import MovieList from "../components/movie-list/MovieList";
import {debounce, getMoviesByName, getPopularMovies} from "../utils";
import SearchForm from "../components/search-form/SearchForm";

const Home = () => {
    const [movies, setMovies] = useState([]);
    const [page, setPage] = useState(1);
    const [searchedTerm, setSearchedTerm] = useState("");

    const onSearch = (value) => {
        setPage(1);
        setSearchedTerm(value);
    }

    useEffect(() => {
        // console.log(page);
        let a;
        const getMovies = async () => {
            if (searchedTerm) {
                a = await getMoviesByName(page, searchedTerm);
                console.log(a);
                if (page === 1) {
                    setMovies(() => a);

                    return;
                }

                setMovies(prevState => [...prevState, ...a]);

                return;
            }

            a = await getPopularMovies(page);

            setMovies(prevState => [...prevState, ...a]);
        }

        getMovies();

    }, [page, searchedTerm]);

    useEffect(() => {
        const debouncedScrollHandler = debounce(scrollHandler, 200);

        window.addEventListener("scroll", debouncedScrollHandler);

        return () => window.removeEventListener("scroll", debouncedScrollHandler);
    }, []);

    const scrollHandler = () => {
        const fetchHeight = document.body.scrollHeight - (window.innerHeight * 2);

        if (window.scrollY >= fetchHeight) {
            // console.log("------")
            setPage(prevState => prevState + 1);
        }
    };

    return (
        <div>
            <Container>
                <h1 className="text-center">Search Movies</h1>
                <SearchForm onSearch={onSearch}/>
                <br/>
                <MovieList movies={movies}/>
            </Container>
        </div>
    );
};

export default Home;