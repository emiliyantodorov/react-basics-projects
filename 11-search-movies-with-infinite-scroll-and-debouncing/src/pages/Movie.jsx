import React, {useEffect, useState} from 'react';
import Container from "react-bootstrap/cjs/Container";
import {baseImgUrl, getMovie} from "../utils";

const Movie = ({match}) => {
    const [movie, setMovie] = useState(null);

    useEffect(() => {
        const getMovieFn = async () => {
            const {title, poster_path, overview} = await getMovie(match.params.id);

            setMovie(prevState => ({title, poster_path, overview}));
        }

        getMovieFn();
    }, [])

    return <div>
        {
            movie &&
            <div>
                <Container>
                    <h2>{movie.title}</h2>
                    <img src={`${baseImgUrl}/w185/${movie.poster_path}`} alt=""/>
                    <p>{movie.overview}</p>
                </Container>
            </div>
        }
    </div>

};

export default Movie;