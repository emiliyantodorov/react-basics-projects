import React from 'react';
import {Switch, Route} from "react-router-dom";
import Home from "./pages/Home";
import Movie from "./pages/Movie";

const App = () => {

    return <div>
        <Switch>
            <Route exact path="/">
                <Home/>
            </Route>
            <Route path="/movies/:id" component={Movie}/>
        </Switch>
    </div>
};

export default App;