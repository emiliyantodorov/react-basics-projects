import React, {useState} from 'react';

const SearchForm = ({onSearch}) => {
    const [value, setValue] = useState("");

    const submitHandler = evt => {
        evt.preventDefault();

        onSearch(value);
    }

    return (
        <div className="text-center">
            <form onSubmit={submitHandler}>
                <input type="text"
                       value={value}
                       onChange={evt => setValue(evt.target.value)}
                />
            </form>
        </div>
    );
};

export default SearchForm;