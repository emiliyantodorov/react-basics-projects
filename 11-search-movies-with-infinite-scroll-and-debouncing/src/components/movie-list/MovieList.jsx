import React from 'react';
import MovieItem from "../movie-item/MovieItem";

const MovieList = ({movies}) => {
    return (
        <section>
            <div className="row">
                {
                    movies.map(curMovie =>
                        <div key={curMovie.id} className="col-12 col-sm-4 col-md-3 py-3 my-2 bg-info">
                            <MovieItem title={curMovie.title} {...curMovie}/>
                        </div>)
                }
            </div>
        </section>
    );
};

export default MovieList;