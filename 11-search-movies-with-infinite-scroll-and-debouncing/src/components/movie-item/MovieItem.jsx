import React from 'react';
import {baseImgUrl} from "../../utils";
import {Link} from "react-router-dom";

const MovieItem = ({id,title, poster_path}) => {
    return (
        <Link to={`/movies/${id}`}>
            <img src={`${baseImgUrl}/w185/${poster_path}`}
                 alt=""
                 className="w-100"
            />
            <h3>{title}</h3>
        </Link>
    );
};

export default MovieItem;