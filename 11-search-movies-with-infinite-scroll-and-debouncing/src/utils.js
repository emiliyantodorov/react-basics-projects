export const URL = "https://api.themoviedb.org/3";
export const apiKey = "d7e639e4dbd285cfc6920ed91f919f53";
export const baseImgUrl = "https://image.tmdb.org/t/p";

export const getPopularMovies = async (page) => {
    try {
        const res = await fetch(`${URL}/movie/popular?api_key=d7e639e4dbd285cfc6920ed91f919f53&language=en-US&page=${page}`);
        const data = await res.json();
        console.log(data);
        return data.results.map(({id, title, poster_path}) => ({
            id, title, poster_path,
        }));
    } catch (err) {
        console.log(err);
    }
}

export const getMoviesByName = async (page, query) => {

    try {
        const res = await fetch(`${URL}/search/movie?api_key=d7e639e4dbd285cfc6920ed91f919f53&language=en-US&query=${query}&page=${page}&include_adult=false`);
        const data = await res.json();

        return data.results.map(({id, title, poster_path}) => ({
            id, title, poster_path,
        }));
    } catch (err) {
        console.log(err);
    }
}

export const getMovie = async id => {
    try {
        const res = await fetch(`${URL}/movie/${id}?api_key=${apiKey}&language=en-US`);
        return await res.json();
    } catch (err) {
        console.log(err);
    }
}

export const debounce = (fn, delay) => {
    let timerId = null;

    return () => {
        if (timerId) {
            clearTimeout(timerId);
        }

        timerId = setTimeout(fn, delay);
    }
}
