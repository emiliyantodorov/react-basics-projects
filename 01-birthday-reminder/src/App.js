import {Container} from "react-bootstrap";
import {useEffect, useState} from "react";
import List from "./components/List";
import Button from "react-bootstrap/Button";

const url = "https://jsonplaceholder.typicode.com";

const App = () => {
    const [users, setUsers] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const getUsers = async (url) => {
        try {
            const res = await fetch(url);

            setIsLoading(false);

            if (!res.ok) {
                setError("Bad Request!");

                return;
            }

            const data = await res.json();

            setError(false);
            setUsers(data)
        } catch (e) {
            console.log(e.message);
        }
    }

    useEffect(() => {
        getUsers(`${url}/users`);
    }, [])

    return (
        <div className="app">
            <section className="section-birthdays">
                <Container>
                    <div
                        className="d-flex flex-column justify-content-center align-items-center bg-info rounded-3 pb-3">
                        <h2 className="p-3 text-center text-white">{users.length} Birthdays Today</h2>
                        {error && <div className="alert-danger text-center">{error}</div>}
                        {isLoading && "Loading..."}
                        {!error && <List items={users}/>}

                        <Button onClick={() => setUsers([])}>Remove All</Button>
                    </div>
                </Container>
            </section>
        </div>
    )
}

export default App;