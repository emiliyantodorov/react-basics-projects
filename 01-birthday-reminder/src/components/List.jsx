import {ListGroup, ListGroupItem} from "react-bootstrap";

const List = ({items}) => {

    return (
        <ListGroup as="ul" className="mb-3">
            {
                items.map(({id, name}) =>
                    <ListGroupItem key={id}

                    >
                        {name}
                    </ListGroupItem>)
            }
        </ListGroup>
    )
}

export default List;